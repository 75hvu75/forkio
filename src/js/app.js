import * as myFunctions from "./modules/functions.js";
import { handleBurgerClick } from './modules/burger.js';

myFunctions.isWebp();
document.addEventListener('DOMContentLoaded', handleBurgerClick);
