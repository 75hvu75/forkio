'use strict';

export function handleBurgerClick() {
    const navbarToggle = document.querySelector('.navbar__toggle');
	const navbar = document.querySelector('.navbar');
	
	navbarToggle.addEventListener('click', () => {
		navbarToggle.classList.toggle('open');
        navbar.classList.toggle('open');
	});
	
	document.addEventListener('click', event => {
        const targetElement = event.target;
		if (!targetElement.closest('.navbar__menu') && !targetElement.closest('.navbar__toggle')) {
			navbarToggle.classList.remove('open');
            navbar.classList.remove('open');
        }
    });
}
